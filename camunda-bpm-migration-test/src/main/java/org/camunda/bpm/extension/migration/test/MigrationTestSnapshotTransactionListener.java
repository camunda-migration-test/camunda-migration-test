package org.camunda.bpm.extension.migration.test;

import java.sql.Connection;
import java.sql.SQLException;

import org.camunda.bpm.engine.impl.cfg.TransactionListener;
import org.camunda.bpm.engine.impl.interceptor.CommandContext;

public class MigrationTestSnapshotTransactionListener implements TransactionListener {

  @Override
  public void execute(CommandContext commandContext) {
    Connection connection = commandContext.getDbSqlSession().getSqlSession().getConnection();
    try {
      connection.createStatement().executeQuery("SCRIPT TO snapshot.sql").close();
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

}
